# Front-End Team D

![logo](https://gitlab.com/binarxglints_batch11/miniproject/team-d/frontendteam/-/raw/83a17ff1091e2a7bc1116f654c215c630a42d6db/Movee1.png)

---

This is a movie review website built using React.JS as a Front-end. It's part of our task at Glints Academy Batch 11 Intensive Bootcamp. It contains the home page, sign-in page, sign-up page, and movie detail page consist of overview, characters and review. 

# Live demo
You could find the live version here: https://movee-review.herokuapp.com

# Project Walkthrough

#  Homepage

![homepage-1](https://gitlab.com/binarxglints_batch11/miniproject/team-d/frontendteam/-/raw/dean/images/homepage-1.png)

![homepage-2](https://gitlab.com/binarxglints_batch11/miniproject/team-d/frontendteam/-/raw/dean/images/homepage-2.png)

![homepage-footer](https://gitlab.com/binarxglints_batch11/miniproject/team-d/frontendteam/-/raw/dean/images/homepage-footer.png)

The Home Page consists of 5 main sections:

1. Header
- Logo: it redirects you to the home page when you click on it.
2. Carousel
- It shows the slide of movie poster
3. Navbar
- It redirects you to a spesific genre of the movie lists
4. Movie cards
- It shows all the movie lists
5. Footer
- It consist of some information page


# Sign-in Page

![sign-in](https://gitlab.com/binarxglints_batch11/miniproject/team-d/frontendteam/-/raw/dean/images/sign-in-baru.png)

Sign-in page consist of:

- App logo

- Email address input field.

- Password input field.

- Sign-in Button: It has a validation option, if any field in the form is empty it will be disabled. If the form fields have any  data it will be active and will send the data to the backend for authentication and authorization using jwt.

- Link to Sign-up Page: it redirects you to the sign-up page.

# Sign-up Page

![sign-up](https://gitlab.com/binarxglints_batch11/miniproject/team-d/frontendteam/-/raw/dean/images/sign-up-baru.png)

Sign-up page, consist of:

- App logo

- Name input field.

- Email address input field.

- Password input field.

- Confirm password field.

- Sign-up Button: It has a validation option, if any field in the form is empty it will be disabled. If the form fields have any data it will be active and will send the data to the backend for registration.

- Link to Sign-in Page: it redirects you to the sign-in page.

# Movie Detail

Clicking one of the movie card will bring you to the movie detail page which has a movie poster, title and synopsis of the movie

![movie-detail](https://gitlab.com/binarxglints_batch11/miniproject/team-d/frontendteam/-/raw/dean/images/movie-detail.png)

---

You could also watch the trailer and find more information about the movie by clicking overview for more detail, characters for cast and review to see what others think about the movie

---

![trailer](https://gitlab.com/binarxglints_batch11/miniproject/team-d/frontendteam/-/raw/dean/images/trailer.png)

---

![overview](https://gitlab.com/binarxglints_batch11/miniproject/team-d/frontendteam/-/raw/dean/images/movie-detail-overview.png)

---

![characters](https://gitlab.com/binarxglints_batch11/miniproject/team-d/frontendteam/-/raw/dean/images/movie-detail-characters.png)

---

![review](https://gitlab.com/binarxglints_batch11/miniproject/team-d/frontendteam/-/raw/dean/images/movie-detail-review.png)

# Technology Used

- React.JS
- React Router
- React Hooks
- JSX
- CSS Modules
- VSCode
- EsLint
- Material UI
- Gitlab

# How to Used


To be able to use this react app locally in a development environment you will need the following:

1. You will need Git and Node.js installed on your computer.
2. Open your terminal and clone this repo
- git clone https://gitlab.com/binarxglints_batch11/miniproject/team-d/frontendteam.git
3. Go to the folder
- cd frontendteam
- cd movie-review-app
4. Install dependencies
- npm install
5. Run the app
- npm start

# Authors

Front-End Team D Glints Academy Batch 11

# License

MIT



