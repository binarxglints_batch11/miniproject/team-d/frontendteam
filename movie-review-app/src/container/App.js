import React from 'react';
import Homepage from '../component/Homepage/Homepage';
import OverviewPage from '../component/OverviewPG/OverviewPage';
// import Characters from '../component/OverviewPG/Characters';
// import Review from '../component/OverviewPG/Review';
import {Route,Switch} from 'react-router-dom';
import './App.css';

function App() {
  return (
    <div>
      {/* <Router> */}
        <Switch>
            <Route exact path="/" component={Homepage} />

            <Route path="/movie/:tabs/:id" component={OverviewPage} />
        </Switch>
      {/* </Router> */}
    </div>
    

    // <Homepage />
    
  );
}

export default App;
