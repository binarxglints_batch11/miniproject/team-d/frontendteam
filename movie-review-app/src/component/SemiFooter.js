import React from 'react';
import './SemiFooter.css';

const SemiFooter = () => {
    return (
        <div className="zone_footer yellow">

            <div className = "footer_grid">
                
                <div className="mainCol1">
                   <img src="/images/Movee1.png" alt="Movee" className = "logoFooter"></img>
                   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard.printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard</p>
                </div>

                <div>
                    <ul className="main-nav1">

                        <p><a href="#" style={{textDecoration:"none"}}>about us</a></p>
                        <p><a href="#" style={{textDecoration:"none"}}>blog</a></p>
                        <p><a href="#" style={{textDecoration:"none"}}>services</a></p>
                        {/* <p><a href="#" style={{textDecoration:"none"}}>career</a></p> */}
                        <p><a href="#" style={{textDecoration:"none"}}>media center</a></p> 
                    </ul>
                </div>
                

                <div>
                    <p>
                        Get It Now!
                    </p>
                    <img src="/images/googleapple1.png" alt=""></img>
                    <p>
                        Find Us!
                    </p>
                    <img src="/images/socialmedia1.png" alt=""></img>
                </div>
            </div>

        </div>
    )
}

export default SemiFooter;