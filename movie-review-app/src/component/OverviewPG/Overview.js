import React from 'react'
import './Overview.css'

const Overview = ({title,synopsis, releaseYear, director, budget, studio}) => {
	return (
		<div className='overview'>
			{/*{console.log({releaseYear})}
			{console.log({poster})}*/}
			<h1 className='judul'>{title}</h1>
			
			<div style={{marginLeft:"1vw",marginRight:"1vw"}}>
				<h1>Synopsis</h1>
				<hr/>
				<p className='synopsis'>{synopsis}</p>
			</div>
			<div style={{marginLeft:"1vw",marginRight:"1vw"}}>
				<h1>Movie Info</h1>
				<hr/>
				<p className='mvdt'>Studio : {studio}</p>
				<p className='mvdt'>Director : {director}</p>
				<p className='mvdt'>Release : {releaseYear}</p>
				<p className='mvdt'>Budget : ${budget}</p>
			</div>
		</div>
	)
}

export default Overview