import React from 'react'
import StarRating from './StarRating'
import './Review.css'

const Review = ({theComment}) => {
	return (
		<div >
			<div className="fontcolor">

				<div style={{marginLeft:"3vw",marginRight:"3vw"}}>
					<h2>Add your review</h2>
					<p>You have to login to your account first to add your review</p>
					<hr/>
				</div>

				<div style={{marginLeft:"3vw",marginRight:"3vw", marginBottom:"6vw"}}> {/* di div buat bagian nambahin komentar tapi login dulu */}
					<h2>Your Name</h2> 
					<StarRating />
					<textarea className='textarea' type="text" placeholder='Leave your review'/>
				</div>

				<div style={{marginLeft:"3vw",marginRight:"3vw"}}>
					<h2>Other Review</h2>
					<hr/>
				</div>
				
				{
					theComment.map(res => ( 
				<div style={{marginLeft:"3vw",marginRight:"3vw",marginBottom:"3vw"}}>
					<h2>{res.userId.username}</h2>
					<p>{res.review}</p>
				</div>
					))
				}

			</div>
			
		</div>
	)
}

export default Review