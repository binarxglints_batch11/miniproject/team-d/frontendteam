import React from 'react';
import Trailer from './Trailer'
import './Jumbotron.css';

const Jumbotron = ({poster, title, synopsis, trailer}) => {
    return(
        <div>
            <div className='jumbo'>
           
                <img className='poster' src={`https://team-d.gabatch11.my.id${poster}`} alt=""/>
                <div>
                        <h1 className='mvdt1'>{title}</h1>
                        <p className='mvdt1'>{synopsis}</p>
                        <Trailer trailer={trailer}/>
                </div>
           
                
            </div>
        </div>
        
    )
}

export default Jumbotron