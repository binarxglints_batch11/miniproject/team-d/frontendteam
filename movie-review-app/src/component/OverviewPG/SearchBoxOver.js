import React, {useState} from 'react';
import ResultSearchCards from './ResultSearchCards'
import './SearchBoxOver.css';  // pengaturan css SearchBox nya ada di HeadNav.css jadi import HeadNav.css

const SearchBoxOver = () => {
	const [query, setQuery] = useState("")
	const [results, setResults] = useState([])

	const onChange = e => {
		e.preventDefault()

		setQuery(e.target.value)

		fetch(`https://team-d.gabatch11.my.id/movie&query=${e.target.value}`)
		.then((res) => res.json())
		.then((data) => {
			
			if (!data.errors) {
				setResults(data.results)
			} else {
				setResults([])
			}
		})
	}

    return(
    	
        <div>
          	<input className="searchBoxStyle" 
            type="search" 
            placeholder="Search Movie"
            value={query}
            onChange={onChange}
            />
          
          {results.length > 0 && (
          	<ul className="results">
          	{results.map(movie => (
          		<li key={movie.id}>
          			<ResultSearchCards movie={movie} />
          		</li>
          		))}
          	</ul>
          	)}
       </div>
    )
}

export default SearchBoxOver