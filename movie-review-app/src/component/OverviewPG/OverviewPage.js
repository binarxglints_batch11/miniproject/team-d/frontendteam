import React,{useState,useEffect} from 'react';
import HeadNav from '../Homepage/HeadNav';
// import HeadNav_ReuseableOver from './HeadNav_ReuseableOver';
import Jumbotron from './Jumbotron';
import FooterCR from '../FooterCR';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Overview from './Overview'
import Characters from './Characters'
import Review from './Review'
import NavBarOver from './NavBarOver'
import SemiFooter from '../SemiFooter'
import LoadingPage from '../Homepage/LoadingPage';
import axios from 'axios';


const OverviewPage = (props) => {

  const [detMv, setDetMv] = useState([])

 
  const getData = async() => {
    // console.log(props.match.params.id)

    const res = await axios.get(`https://team-d.gabatch11.my.id/movie/detail/${props.match.params.id}`,{headers: {"Access-Control-Allow-Origin": "*"}})
    
    const detMv = res.data;
    console.log(detMv)
            //   const sliceMv = movie.slice(offset, offset + perPage)
              const sliceMv = detMv.result
              const postData = sliceMv 
              setDetMv(postData)
}

  useEffect(() => {
    getData()
    // getDataPoster()
  }, [])


    // const id = props.match.params.id
    // const title = props.location.state.title
    // const releaseYear = props.location.state.releaseYear
    // const _id = props.match.params._id
    // const synopsis = props.location.state.synopsis
    // const category = props.location.state.category
    // const trailer = props.location.state.trailer
    // const poster = props.location.state.poster
    
    

    // dibawah ada cara ke 2
    return(
        <div>
            <HeadNav/>
            {/* <HeadNav_ReuseableOver/> */}
            <Jumbotron 
            poster={detMv.poster}
            title={detMv.title}
            synopsis={detMv.synopsis}
            trailer={detMv.trailer}

            />
            <Router>
            <NavBarOver id={props.match.params.id}

            />
      		<Switch>
      			<Route exact path='/movie/detail/:id'>
            {detMv.length === 0 ?
                <LoadingPage/>
                                    :
      				<Overview
                      title={detMv.title}
                      synopsis={detMv.synopsis}
                      releaseYear={detMv.releaseYear}
                      director={detMv.director}
                      budget={detMv.budget}
                      studio={detMv.studio}
                      // category={detMv.category}
                      />
            }
      			</Route>

      			<Route exact path='/movie/character/:id'>
            {detMv.length === 0 ?
                <LoadingPage/>
                                    :
      				<Characters
                      title={detMv.casts}
                      />
            }
      			</Route>

      			<Route exact path='/movie/review/:id'>
            {detMv.length === 0 ?
                <LoadingPage/>
                                    :
      				<Review
                      theComment={detMv.reviews}
                      // commentator={detMv.reviews.userId.username}
                      // theComment={detMv.reviews.review}

              />
            }
      			</Route>
            
      		</Switch>
      		</Router>
          <SemiFooter/>
          <FooterCR/>
              
        </div>
    )
}

export default OverviewPage