import React from 'react';
import './HeadNav_ReuseableOver.css';
import SearchBoxOver from './SearchBoxOver';

// kita tidak timport pic karena kita taro pic nya di root folder (public) didalam images folder
const HeadNav_ReuseableOver = () => {
    return(
        <div className="zone green sticky">
            <ul className="main-nav">
                <li><img src="/images/Movee1.png" className="redkotak1" alt="Logo"/></li> 
                {/*<li>ANIMENIA TV</li>*/}
                <li><SearchBoxOver /></li>
                <li className="push"><a href="#"></a>PHOTO</li>
            </ul>
        </div>
    )
}

export default HeadNav_ReuseableOver;