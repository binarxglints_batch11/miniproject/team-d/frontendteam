import React from 'react'
// import {Link} from 'react-router-dom'
import {Link,Switch, Route,useParams} from 'react-router-dom'
import './NavBarOver.css'
 
const NavBarOver = ({id}) => {
	return (
		<div className="zone green lebarFull">
		<ul className="main-nav">
			{/* <li >
				<Link className='li1' to={{
					pathname:`/movie/detail/${_id}`,
					state:{
						_id: {_id},
						title: {title},
						synopsis: {synopsis},
						releaseYear: {releaseYear},
						category: {category},
						poster: {poster}

					}
				}}>Overview</Link>				
			</li> */}

			<li>
				<Link className="menu_category" to={`/movie/detail/${id}`}>Overview</Link>
				{/* <Link className='li2' to={`/movie/detail/${id}`}>Overview</Link> */}
				{/* <Link className='li2' to="/movie/character/">Characters</Link> */}
			</li>

			<li>
				<Link className="menu_category" to={`/movie/character/${id}`}>Characters</Link>
				{/* <Link className='li2' to="/movie/character/">Characters</Link> */}
			</li>

			<li>
				<Link className="menu_category" to={`/movie/review/${id}`}>Review</Link>
				{/* <Link className='li3' to="/movie/review/">Review</Link> */}
			</li>
		</ul>
			{/* <Switch>
				<Route path="/:id" children={<Child />} />
			</Switch> */}
		</div>
	
	)
}

// function Child() {
// 	// We can use the `useParams` hook here to access
// 	// the dynamic pieces of the URL.
// 	let { id } = useParams();
  
// 	return (
// 	  <div>
// 		<h3>ID: {id}</h3>
// 	  </div>
// 	);
//   }

export default NavBarOver