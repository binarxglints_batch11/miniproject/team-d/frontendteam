import React from 'react'
import './ResultSearchCards.css'

const ResultSearchCards = ({movie}) => {
	return (
		<div className='result-card'>
			<div className='poster-wrapper'>
				{movie.poster_path ? (
					<img src={`https://image.tmdb.org/t/p/w200${movie.poster_path}`} 
					alt={`${movie.title} Poster`}
					/>
					) : (
						<div className='filler-poster'></div>
					)}
			</div>
		
				<div>
					<h3>{movie.title}</h3>
					<h4>{movie.release_date}</h4>
					<button>ADD TO WATCHLIST</button>
				</div>
			


		</div>
	)
}

export default ResultSearchCards