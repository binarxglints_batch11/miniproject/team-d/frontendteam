import React from 'react';
// import React,{useState} from 'react';
import Card from './Card';
import './Card-N-CardList.css';
import Pagination from './Pagination';

const CardList = ({mv,totalPage,handlePageClick}) => {


    return(
        <div>
            <div className="card-temp">
                {
                    mv.map((user,i)=>{
                    console.log(mv);
                        return (
                            <Card key={i} 
                                  id={mv[i].id} 
                                  title={mv[i].title}
                                  releaseYear={mv[i].releaseYear}
                                  _id={mv[i]._id} 
                                  synopsis={mv[i].synopsis}
                                  category={mv[i].category}
                                  trailer={mv[i].trailer}
                                  poster={<img src={`https://team-d.gabatch11.my.id${mv[i].poster}`} alt=""/>}/>
                        )
                    })
                }
            </div>
            <Pagination totalPage={totalPage} handlePageClick={handlePageClick}/>
        </div>

    )
}
export default CardList;