import React,{useState,useEffect} from 'react';
import axios from 'axios'
// import ReactPaginate from 'react-paginate';
import HeadNav from './HeadNav';
import Carousel from './Carousel';
import NavCategory from './NavCategory';
import LoadingPage from './LoadingPage';
import CardList from './CardList';
import SemiFooter from '../SemiFooter';
import FooterCR from '../FooterCR';
import ErrorBoundary from './ErrorBoundary';
// import ModalLoginJWT from '../Modal/ModalLoginJWT';
// import {BrowserRouter} from 'react-router-dom';

// import {Route} from 'react-router-dom';


const Homepage = () => {

    const [movie,setMovie] = useState ([])   // data nya 
    // const [offset, setOffset] = useState(0);  // offset dipake nentuin range halaman
    const [perPage] = useState(10);  // jumlah item per halaman
    const [pageCount, setPageCount] = useState(0)  // total jumlah halaman (ini buat menentukan berapa banyak halaman yang dibuat)
    const [searchMovie, setSearchMovie] = useState ('')
    const [activePage, setActivePage] = useState (0)
    const [categoryName, setCategoryName] = useState([]);

    const totalPage = pageCount;

    const visitedPage = pageCount * perPage;
    // const [searchField, setSearchField] = useState ('')
    
    const getData = async() => { 

        const res = await axios.get(`https://team-d.gabatch11.my.id/movie/${activePage}`,{headers: {"Access-Control-Allow-Origin": "*"}})

            setMovie(res.data.result)
            setPageCount(res.data.totalPage)       
    }

    const handlePageClick = (e) => {   // ini handleklik nanti dikirim ke Pagination
        const selectedPage = e.selected;
        setActivePage(selectedPage)  
    };

    const onSearchMv = (event) =>{    //event disini emang paramenter tapi biasanya berguna buat onChange DOM event
        setSearchMovie (event.target.value);  // event.target.value wajib dipake untuk ambil nilai dari apa yg ada di textboxt pada kondisi terkini,...nah untuk kasus ini di assign
    }

    useEffect(() => {
        getData()
        // getDataPoster()
      }, [activePage])

    //   const filterMovie = movie

    const filterMovie = {
        title: movie.filter(contentInMovie=>{
                return contentInMovie
                .title.toLocaleLowerCase().includes(searchMovie.toLocaleLowerCase())
            }),
        category: movie.filter(contentInMovie=>{
            return contentInMovie
            .category
        })
    }

    // const filterMovie = movie.filter(contentInMovie=>{
    //     return contentInMovie
    //     .title.toLocaleLowerCase().includes(searchMovie.toLocaleLowerCase())
    // })



    const getCategory = async () => {  // fetching buat category 
        const res = await axios.get(`https://team-d.gabatch11.my.id/category`)
            
            setCategoryName(res.data.data)
            console.log(categoryName);

    }

    useEffect(() => {
        getCategory()
        
      }, [])

      const filterCategory = categoryName.filter(contentInCategory => {
        return contentInCategory._id
    })

    const fetch = async (categoryName) => {
        const url = categoryName
          ? `https://team-d.gabatch11.my.id/movie/category/${categoryName}/${activePage}`
          : `https://team-d.gabatch11.my.id/movie/${activePage}`;
        const data = await axios.get(url);
        console.log(data);
        // set portfolio data to state
        setMovie(data.data.result);

        // setPageCount(data.data.totalPage + 1)
      };


    return(
        <div>
            <Carousel/>
            <HeadNav onSearchMv={onSearchMv}/>
                <h2 style={{marginLeft:'2.5em', color:'white'}}>Browse by category</h2>
            <NavCategory 
                fc={filterCategory}
                // onChangeCategory={onChangeCategory}
                onChangeCategory={(id) => {
                    // setActivePage(0);
                    fetch(id);
                  }}
                />
            <br style={{marginTop:'20px'}}></br>
            {/* <CardList mv={movie} totalPage={totalPage} handlePageClick={handlePageClick}/> */}

            <ErrorBoundary>

                {movie.length === 0 ?
                <LoadingPage/>
                                    :
                <CardList mv={filterMovie.title} totalPage={totalPage} handlePageClick={handlePageClick}/>
                }
            </ErrorBoundary>
            
            <SemiFooter/>
            <FooterCR/>
        </div>
    )
}

export default Homepage