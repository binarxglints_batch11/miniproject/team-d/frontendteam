import React from 'react';
// import React,{useState, useEffect} from 'react';
// import axios from 'axios'
import './Card-N-CardList.css';
import {Link} from 'react-router-dom';

const Card = (props) => {

    return(

        <div>
            <Link to={{
                        pathname:`/movie/detail/${props._id}`,
                        state: {
                           id: `${props.id}`,
                           title: `${props.title}`,
                           releaseYear: `${props.releaseYear}`,
                           _id: `${props._id}`,
                           synopsis: `${props.synopsis}`,
                           category: `${props.category}`,
                           trailer: `${props.trailer}`,
                           poster: `${props.poster}`
                        }
                    }} style={{ textDecoration: 'none', color:'white' }}>

            <div className="box">
                {props.poster}
                <div className="text-align">
                    <h2>{props.title}</h2>
                    <h3>{props.year}</h3>
                </div>
            </div>
            </Link>
        </div>
    )
}
export default Card;