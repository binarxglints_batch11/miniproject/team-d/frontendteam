import React from 'react';
import './Pagination.css';
import ReactPaginate from 'react-paginate';

const Pagination = ({totalPage, handlePageClick}) => {

    return(
        <div className="pagination">
            <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    // breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={totalPage}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
        </div>
    )
}

export default Pagination;