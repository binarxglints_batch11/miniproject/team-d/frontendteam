import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './Carousel.css';

const Carousel = () => {

    const settings = {
        dots: true,
        fade:true,
        autoplay: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        
      };

    return(

// cara ke 2 cara normal

        <div className="container">
            <Slider {...settings}>
                <div>
                    <img src="/images/thor-ragnarok.jpeg" alt="Thor Ragnarok"/>
                </div>
                <div>
                    <img src="/images/iron-man.jpg" alt="Saint Seiya"/>
                </div>
                <div>
                    <img src="/images/avengers_endgame.jpg" alt="Avenger Endgame"/>
                </div>
            </Slider>
        </div>
    )
}

export default Carousel