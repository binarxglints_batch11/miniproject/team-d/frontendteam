import React from 'react';
import './LoadingPage.css';

const LoadingPage = () => {
    return(
        <div className="zone-loading">
                    <h1>Loading...</h1>
        </div>
    )
}

export default LoadingPage