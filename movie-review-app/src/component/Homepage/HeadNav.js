import {useState,useEffect} from 'react';
// import React from 'react';
import './HeadNav.css';
import SearchBox from '../SearchBox';
import OnOffModal from '../Modal/OnOffModal';
import {Link} from 'react-router-dom'
import ContModal from '../Modal/ContModal';


// kita tidak timport pic karena kita taro pic nya di root folder (public) didalam images folder
const HeadNav = ({onSearchMv}) => {

    const {isShowing, toggle, isSignIn, tgl, logout} = OnOffModal(); //getEmail

    const [getEmail, setGetEmail] = useState(""); 

    

    useEffect(() => {
        const getUserEmail = JSON.parse(localStorage.getItem('user'));
        if(getUserEmail) {
            console.log(getUserEmail)
            setGetEmail(getUserEmail?.email); 
            // window.location.reload();
        }
    },[])
    // window.location.reload();

    return(
        <div>
            <div className="zone_headNav green sticky">
                <ul className="main-nav">
                    <li><Link to="/"><img src="/images/Movee1.png" className="logo" alt="Logo"/></Link></li> 
                    <li><SearchBox onSearchMv={onSearchMv} /></li>
                    {
                        isSignIn ? <ul className="main-nav push">
                                        {/* {JSON.parse (localStorage.getItem('user'));} */}
                                        <div className="push disflex">
                                            <li>{getEmail} {console.log(getEmail)}</li>
                                            <li onClick={logout}>Sign Out</li>
                                        </div>
                                    </ul>  //ini kalo kita sign in
                                          : <li className="push" onClick={toggle}>Sign In</li>  // ini kalo kita gak sign in
                    }
                    
                </ul>
            </div>
                <ContModal
                    isShowing={isShowing}
                    hide={toggle}
                    tgl={tgl}
                />
        </div>
        
    )
}

export default HeadNav;