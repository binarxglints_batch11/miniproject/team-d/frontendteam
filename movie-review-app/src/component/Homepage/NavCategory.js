import React from 'react';
import './HeadNav.css';


const NavCategory = ({fc,onChangeCategory}) => {
       
        return(
           
                <div className="zone green lebarFull" >
                    <ul className="main-nav">
                    {fc.map((user,i)=>{
                    return (
                        <div key ={fc[i]._id}>
                            <li className="menu_category" onClick={() => onChangeCategory(`${fc[i]._id}`)}>{fc[i].name}</li> 
                        </div>
                        
                        // <li className="menu_category" onClick={() => onChangeCategory("id")}>Anime</li>
                        // <li className="menu_category" onClick={() => onChangeCategory("id")}>Action</li>
                        // <li className="menu_category" onClick={() => onChangeCategory("id")}>Adventure</li>
                        // <li className="menu_category" onClick={() => onChangeCategory("id")}>Science</li>
                        // <li className="menu_category" onClick={() => onChangeCategory("id")}>Commedy</li> 
                    )})
                    }
                    </ul>
                </div>
                
            )
        
}
    
export default NavCategory