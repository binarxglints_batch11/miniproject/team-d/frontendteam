import React from 'react';
import './Footer.css';

const FooterCR = () => {
    return (
        <div className="flex">
            <footer className="zone yellow text-align"><h1>Copyright &copy; 2021 MOVEE. All Rights Reserved</h1></footer>
        </div>
    )
}

export default FooterCR;