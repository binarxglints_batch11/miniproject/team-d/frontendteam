import React, { useState } from "react";
import axios from 'axios'

import './SignInUpModal.css'


const SignInModal = ({ onRouteChange, hide, tgl }) => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function validateForm() {
    return email.length > 0 && password.length > 8;
  }


  function handleSubmit(event) {
    
    hide();
    tgl(); 

    axios.post('https://team-d.gabatch11.my.id/auth/signin',{
        email: email,
        password: password
    }).then(function (res){
        console.log(res)
        localStorage.setItem('token', res.data.token);
        localStorage.setItem('user', res.config.data);
        // localStorage.setItem("user", JSON.stringify(res.config.data));

    }).catch(function (err){
        console.log(err)
    })

    // event.preventDefault();
  }


    return(
        
        <div>

                <main className="pa4 black-80">
                    <div className="measure center">  {/*  div baris ini pengganti form,..tadinya form*/ }    {/*onSubmit={handleSubmit}*/}
                        <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                        <legend className="f4 fw6 ph0 mh0" ><img src="/images/Movee1.png" className="logox" alt="Logo"/></legend>
                        <legend className="f4 fw6 ph0 mh0" >Sign In</legend>
                        <div className="mt3">
                            <label className="db fw6 lh-copy f6" htmlFor="email-address">Email</label>
                            <input className="pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100 txt-color" type="email" name="email-address" placeholder="Enter your email" id="email-address" onChange={(e) => setEmail(e.target.value)}/>
                        </div>
                        <div className="mv3">
                            <label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
                            <input className="b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100 txt-color" type="password" name="password" placeholder="Min length 8 characters, 1 Capital Leter, 1 number and 1 symbol" id="password" onChange={(e) => setPassword(e.target.value)}/>
                        </div>
                        <label className="pa0 ma0 lh-copy f6 pointer"><input type="checkbox"/> Remember me</label>
                        </fieldset>
                        <div className="">
                            <input className="b ph3 pv2 input-reset ba b--black grow pointer f6 dib bt-color" 
                                    type="button" 
                                    // type="submit" 
                                    value="Sign in" 
                                    disabled={!validateForm()}
                                     // onClick={handleSubmit}/> 

                                    
                                     onClick={() => {
                                        // hide();
                                        // tgl();
                                        handleSubmit();
                                    }}/> 

                        </div>
                        <div className="lh-copy mt3">
                            <a className="f6 link dim black db pointer" onClick={onRouteChange}>Sign up</a>
                            <a href="#0" className="f6 link dim black db">Forgot your password?</a>
                        </div>

                    </div>  {/* pengganti form,..tadinya form*/ }
                </main>
            
        </div>
    )
}

export default SignInModal