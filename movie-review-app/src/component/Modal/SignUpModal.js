import React, { useState } from "react";
import Modal from "react-modal";
import axios from 'axios'
// import OnOffModal from '../Modal/OnOffModal';

import './SignInUpModal.css'

Modal.setAppElement("#root");

const SignUpModal = ({ onBackToSignIn,hide,isShowingSU, hideSU }) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPwd, setConfirmPwd] = useState("");


  const confirmPass = confirmPwd

  const notice = () => {
      alert("Silahkan kembali login dengan user dan password yang sudah Anda daftarkan !")
  }

  function validateForm() {
    return (email.length > 0 && 
            password.length > 8 && 
            password === confirmPwd)
  }

  function handleSubmit() {
    axios.post('https://team-d.gabatch11.my.id/auth/signup',{
        name: name,
        email: email,
        password: password,
        confirmPassword: confirmPwd
    }).then(function (res){
        console.log(res)
        localStorage.setItem('token', res.data.token);
        localStorage.setItem('user', res.config.data);
    }).catch(function (err){
        console.log(err)
    })
    // event.preventDefault();
  }

//   function handleSubmit(event) {
//     event.preventDefault();
//   }

    return(
        <div>
            
                <main className="pa4 black-80">
                    <div className="measure center">  {/*onSubmit={handleSubmit}*/}
                        <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                        <legend className="f4 fw6 ph0 mh0" ><img src="/images/Movee1.png" className="logox" alt="Logo"/></legend>
                        <legend className="f4 fw6 ph0 mh0" >Sign Up</legend>
                        <div className="mt3">
                            <label className="db fw6 lh-copy f6" htmlFor="email-address">Name</label>
                            <input className="pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100 txt-color" type="text" name="name" placeholder="Enter your name"  id="name" onChange={(e) => setName(e.target.value)}/>
                        </div>
                        <div className="mt3">
                            <label className="db fw6 lh-copy f6" htmlFor="email-address">Email</label>
                            <input className="pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100 txt-color" type="email" name="email" placeholder="Enter your email"  id="email" onChange={(e) => setEmail(e.target.value)}/>
                        </div>
                        <div className="mv3">
                            <label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
                            <input className="b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100 txt-color" type="password" name="password" placeholder="Min length 8 characters, 1 Capital Leter, 1 number and 1 symbol"  id="password" onChange={(e) => setPassword(e.target.value)}/>
                        </div>
                        <div className="mv3">
                            <label className="db fw6 lh-copy f6" htmlFor="password">Confirm Password</label>
                            <input className="b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100 txt-color" type="password" name="confirmPassword" placeholder="Must same with the password"  id="confirmPassword" onChange={(e) => setConfirmPwd(e.target.value)}/>
                        </div>
                        {/* <label className="pa0 ma0 lh-copy f6 pointer"><input type="checkbox"/> Remember me</label> */}
                        </fieldset>
                        <div className="">
                        <input className="b ph3 pv2 input-reset ba b--black grow pointer f6 dib bt-color" 
                                type="button"
                                // type="submit" 
                                value="Sign Up" 
                                disabled={!validateForm()}
                                onClick={() => {
                                    notice();
                                    onBackToSignIn();
                                    handleSubmit();
                                }}/>
                                {/* // onClick={onBackToSignIn}/> */}
                                {/* // onClick={hide}/> */}
                        </div>
                        <div className="lh-copy mt3">

                        <a className="f6 link dim black db pointer" onClick={onBackToSignIn}>Sign in</a>

                        </div>
                    </div>
                </main>

        </div>
    )
}

export default SignUpModal