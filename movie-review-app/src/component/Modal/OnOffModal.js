import {useState,useEffect} from 'react';
// import SignInModal from './SignInModal';

const OnOffModal = () => {
    const [isShowingSU, setIsShowingSU] = useState(false);

    const [isShowing, setIsShowing] = useState(false);

    const [isSignIn, setIsSignIn] = useState(false); 

    // const [getEmail, setGetEmail] = useState(""); 


    // const getUserEmail = JSON.parse(localStorage.getItem('user'));

    // useEffect(() => {
    //     // const getUserEmail = JSON.parse(localStorage.getItem('user'));
    //     // const getUserEmail = localStorage.getItem('user')
    //     const getUserEmail = JSON.parse(localStorage.getItem('user'));
    //     if(getUserEmail) {
    //         setGetEmail(getUserEmail.email); 
    //     }
    // },[])
    
    // },[localStorage.getItem("user")]) 


    useEffect(() => {
        // const getUserEmail = JSON.parse(localStorage.getItem('user'));
        const local = localStorage.getItem('token')
        if(local) {
            setIsSignIn(true);
            // setGetEmail(getUserEmail);  
        }
    },[])

    const logout = () => {
        localStorage.clear();
        setIsSignIn(false);
        // setIsSignIn(!isSignIn);
        // setGetEmail("")
    }

    const tgl = () => {
        setIsSignIn(!isSignIn);
        // window.location.reload();
    }

    const toggle = () => {
        setIsShowing(!isShowing)        
    }

    const toggleSU = () => {
        if(isShowingSU === false) {
            return (
                    setIsShowing(!isShowing),
                    setIsShowingSU(!isShowingSU)
                    // setIsShowing(!isShowing)
                    );
        } else {
            return (
                setIsShowingSU(!isShowingSU)
                // setIsShowing(!isShowing)
            )
        }
        
    }

    return {
        isShowingSU,
        toggleSU,
        isShowing,
        toggle,
        isSignIn,
        tgl,
        logout,
        // getEmail
        // getUserEmail
    }
};

export default OnOffModal
