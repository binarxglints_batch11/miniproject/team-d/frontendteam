import React, { useState } from "react";
import Modal1 from "react-modal";
import SignInModal from './SignInModal';
import SignUpModal from './SignUpModal';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'

import './ContModal.css'

const ContModal = ({ isShowing, hide, tgl }) => {

    const [showSI, setShowSI] = useState("true");
    // const [isSignIn, setIsSignIn] = useState("false");


    const onRouteChange = () => {
        setShowSI("SignUp")
    }

    const onBackToSignIn = () => {
        setShowSI("true")
    }
 
    return (
        <div>
            <Modal1
                isOpen={isShowing}
                onRequestClose={hide}
                contentLabel="My dialog"
                className="mymodal"
                overlayClassName="myoverlay"
                // closeTimeoutMS={500}
            >
                <div>
                    <Switch>
                        {showSI === "true" ?
                                <SignInModal tgl={tgl} hide={hide} onRouteChange={onRouteChange}/> : <SignUpModal hide={hide} onBackToSignIn={onBackToSignIn}/>}
                    </Switch>
                </div>
            </Modal1>
        </div>
    )
}

export default ContModal;