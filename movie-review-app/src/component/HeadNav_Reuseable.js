import React from 'react';
import './HeadNav_Reuseable.css';
import SearchBox from './SearchBox';

// kita tidak timport pic karena kita taro pic nya di root folder (public) didalam images folder
const HeadNav_Reuseable = () => {
    return(
        <div className="zone green sticky">
            <ul className="main-nav">
                <li><img src="/images/Logo.png" className="redkotak" alt="Logo"/></li> 
                <li>ANIMENIA TV</li>
                <li><SearchBox /></li>
                <li className="push"><a href="#"></a>PHOTO</li>
            </ul>
        </div>
    )
}

export default HeadNav_Reuseable;