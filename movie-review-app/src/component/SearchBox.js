import React from 'react';
import './SearchBox.css';  // pengaturan css SearchBox nya ada di HeadNav.css jadi import HeadNav.css

const SearchBox = ({onSearchMv}) => {
    return(
        <div>
            <input className="searchBoxStyle" type="search" placeholder="Search Movie" onChange={onSearchMv}/>
        </div>
    )
}

export default SearchBox